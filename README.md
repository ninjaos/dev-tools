dev-tools
===========

Miscellaneous scripts and utilities used for the development of Ninja OS

getbshash.sh - get the hash of the bootsector of target block device.

eth\_addr\_scraper.py - dump mac addresses from packet capture.

mk\_eth_\shortlist.sh  - takes a list of ethernet addresses and searches
wireshark's manuf file for matching entries. output to STDOUT or file, same
format as manuf. i.e. a shortlist
