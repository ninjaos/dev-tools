#!/usr/bin/env bash

IN_MANUF="/usr/share/wireshark/manuf"

help_and_exit(){
  cat 1>&2 << EOF
Search wireshark's manuf file for entries match a list of ethernet addresses.
Input is a list of ethernet addresses in a file, one per line, as output by
eth_addr_scrapper.py, and outputs the lines from wiresharl's manuf file that
match. If second parameter is specified it is a file to write output. Otherwise
it is just dumped to STDOUT.

	USAGE:
	mk_eth_shortlist.sh <eth_addr_scrapper outfile> (output file)

EOF
  exit 4
}

message(){
  echo 1>&2 "mk_eth_shortlist.sh: ${@}"
}

submsg(){
  echo 1>&2 "[+]	${@}"
}

exit_with_error(){
  echo 1>&2 "mk_eth_shortlist.sh: ERROR: ${2}"
  exit ${1}
}

main() {
   local in_file="${1}"
   local out_file="${2}"
   
   [ -z ${in_file} ] && help_and_exit
   [ ! -f "${IN_MANUF}" ] && exit_with_error 1 "Could Not Find wireshark's manuf file. Is wireshark installed?"
   [ "${1}" == "--help" ] && help_and_exit
   
   message "Grep'ing wireshark manuf file for VID matches"
   
   local in_list=$(cat ${in_file} | cut -d "#" -f 1 | cut -d ":" -f 1-3) || \
      exit_with_error 1 "Cannot read ${in_file}, check permissions!"
   
   [ ! -z $out_file ] && submsg "Writing output to ${out_file}"
   for mac in ${in_list};do
     if [ ! -z $out_file ];then
       grep ${mac^^} "${IN_MANUF}" >> "${out_file}"
      else
        grep ${mac^^} "${IN_MANUF}"
     fi
   done
}

main "${@}"
