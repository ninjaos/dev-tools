#!/usr/bin/env python
about='''eth_addr_scraper.py:
output a line deliminated list of MAC addresses from either a pcap file,
or from live capture. Capturing interface and invalid MACs like
broadcast and network are stripped out. Default action is to capture
from first network interface, and dump it to STDOUT
'''
import sys
import argparse
import signal
##  External deps. Make sure you have these installed
import netifaces
from generate_mac import generate_mac
from scapy.all import rdpcap,AsyncSniffer
import scapy.layers

# Defaults are set here. this is overwritten by argparse command input
options = {
    'num_packets' : None,
    'iface'       : None,
    'method'      : "capture",
    'in_file'     : None,
    'out_file'    : None,
    'bad_macs'    : set()
}
class colors:
    '''pretty terminal colors'''
    reset='\033[0m'
    bold='\033[01m'
    red='\033[31m'
    cyan='\033[36m'
    yellow='\033[93m'
    green='\033[32m'

def message(message):
    print(colors.bold + "eth_addr_scrape.py: " + colors.reset + message)

def submsg(message):
    print("[+]\t" + message)

def exit_with_error(exit,message):
    print(colors.bold + "eth_addr_scrape.py: " + colors.red + " ¡ERROR!: " + colors.reset + message, file=sys.stderr)
    sys.exit(exit)

def warn(message):
    print(colors.bold + "eth_addr_scrape.py: " + colors.yellow + "¡WARN!: " + colors.reset + message, file=sys.stderr)
    return

def is_valid_iface(iface):
    '''check if interface is valid'''
    out_list = []
    
    # Get a list of all interfaces
    int_list = netifaces.interfaces()
    # remove the loopback
    del( int_list[ int_list.index('lo') ] )
    # check if interfaces supplied are valid
  
    if iface in int_list:
        return True
    else:
        return False

def get_macs_from_file(pcap_file):
    '''get list of MAC addresses from pcap/pcapng file'''
    try:
        packets  = rdpcap(pcap_file)
    except:
        raise OSError("gen_mac_list_from_pcap: " + "Could not read file as PCAP")
    
    mac_list = parse_pcap(packets,options['bad_macs'])
    return mac_list

def parse_pcap(packets,ignore_macs=None):
    '''Parse from pcap loaded into memory either from a file or direct capture. returns a list[] of unique MAC addresses. two options, packets, the pcap capture, and a list[], of ignored MACs(optional)'''
    
    bad_macs = set( {'ff:ff:ff:ff:ff:ff', '00:00:00:00:00:00'} )
    mac_list = set()
    
    # check if iface_mac is really a MAC address
    if ignore_macs != None:
        for mac in ignore_macs:
            if generate_mac.is_mac_address(mac) == True:
                bad_macs.add(mac)

    # If there is no data, just return None
    if packets == None:
        return None
        
    # populate list based on mac addresses in the pcap file
    for pkt in packets:
        # if is ethernet packet
        if type(pkt) == scapy.layers.l2.Ether:
            packet_macs = [pkt.src, pkt.dst]
        # if the packet is a radiotap header(from wireless monitor mode)
        elif type(pkt) == scapy.layers.dot11.RadioTap:
            packet_macs = [pkt.payload.addr1, pkt.payload.addr2, pkt.payload.addr3]

        for mac in packet_macs:
            if mac not in bad_macs and generate_mac.is_mac_address(mac):
                mac_list.add(mac)
    
    mac_list = list(mac_list)
    return mac_list
    
def get_macs_from_capture(iface,iface_type="wired",max_packets=None):
    mac_list = []
    # generate a list of addresses to exclude
    for mac in options['bad_macs']:
        bad_macs.add(mac)
    # get MAC address from interface
    try:
        iface_mac = netifaces.ifaddresses(iface)[17][0]['addr']
        bad_macs  = {iface_mac}
    except:
        exit_with_error(4,iface + " is not a valid ethernet networking interface!")
    
    # If wireless, put interface in monitor mode, so connections are not needed
    if iface_type == "wireless":
       warn("wireless specific code not coded yet")
       #TODO: ¯\_(ツ)_/¯

    # set up async sniffer
    if type(max_packets) == int:
        packet_obj = AsyncSniffer(count=max_packets,iface=iface)
    else:
        packet_obj = AsyncSniffer(iface=iface)
    
    # print banner and alert user
    if max_packets == None:
        submsg("Grabbing packets from " + iface + " - ^C to stop")
    else:
        submsg("Grabbing packets from " + iface + " limit: " + str(max_packets) + " packets - ^C to stop")

    # interrupt, if we get ^C at keyboard, stop packets
    signal.signal(signal.SIGINT, lambda s, f: packet_obj.stop() )
    try:
        packet_obj.start()
    except PermissionError:
        exit_with_error(1,"Do not have permissions for capture, you need root or capture capabilitires.")
    
    packet_obj.join() # wait for the sniffer to finnish
    # Proccess packets and return them
    print("")
    print(packet_obj.results)
    mac_list = parse_pcap(packet_obj.results,bad_macs)
    return mac_list
    
def main():
    mac_list = []
    
    # yarg, get ye argggs
    parser = argparse.ArgumentParser(description=about)
    parser.add_argument('-o', '--output', type=str, nargs=1, help="Output File. Write to a file instead of STDOUT")
    parser.add_argument('-e', '--exclude_macs', type=str, nargs=1, help="Comma seperated list of MAC addresses to exclude in output. When capturing, interface adddress is excluded automaticly")
    parser.add_argument('-n', '--num_packets', type=int, nargs=1, help="Number of packets to capture, if using network interface")
    prog_method = parser.add_mutually_exclusive_group()
    prog_method.add_argument('-i', '--interface', type=str, nargs=1, help="Network Interface. Specify name of network interface to capture packets from(Needs root or capture privleges)")
    prog_method.add_argument('-f', '--file', type=str, nargs=1, help="PCAP file. instead of capturing packets from the network, read from a packet capture file")
    args = parser.parse_args()
    
    # parse ye arggs
    if args.output != None:
        options['out_file']     = args.output[0]
    if args.num_packets != None:
        options['num_packets']  = args.num_packets[0]
    if args.interface != None:
        options['method']       = 'capture'
        if is_valid_iface(args.interface[0]) != True:
            exit_with_error(4,options['iface'] + "is not a valid network interface, quitting")
        else:
            options['iface']    = args.interface[0]
    if args.file != None:
        options['in_file']  = args.file[0]
        options['method']   = 'file'
    if args.exclude_macs != None:
        options['bad_macs'] = set( args.exclude_macs[0].split(',') )

    # Banner
    # Get our MACs
    if options['method'] == 'file':
        # Get MACs from capture file
        message('Dumping MAC addresses from file ' + options['in_file'])
        try:
            mac_list = get_macs_from_file(options['in_file'])
        except OSError:
           exit_with_error(2,options['in_file'] + ": File Not Found!")

    elif options['method'] == 'capture':
       # Get MACs from live network capture
       if options['iface'] == None:
           # if no interface is specified, use first interface on the list as the "default"
           try:
               iface_list = netifaces.interfaces()
               del( iface_list[ iface_list.index('lo') ] )
               options['iface'] = iface_list[0]
           except:
               exit_with_error(2,"No Default Interface, quitting")
       message('Grabbing MAC addresses from network')

       mac_list = get_macs_from_capture(options['iface'],max_packets=options['num_packets'])
    else:
        exit_with_error(2,"Invalid mode, this should not happen, debug")
    
    # Output
    if mac_list == None:
        exit_with_error(1,"Could not get any MAC addresses")
    else:
        output = "\n".join(mac_list)
    
    if options['out_file'] == None:
        print(output)
    else:
        submsg("Writing to " + options['out_file'])
        try:
            file_obj = open(options['out_file'],"w")
            file_obj.write(output)
            file_obj.close()
        except:
            exit_with_error(1,"Could not write to " + options['out_file'])
    
if __name__ == "__main__":
    main()
